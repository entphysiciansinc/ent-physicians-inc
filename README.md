For over 35 years, ENT Physicians Inc. have provided medical and hearing health care to families in the Toledo community. Our experienced team consists of Physicians, Certified Nurse Practitioners, Audiologists and Allergy Specialists.

Address: 3829 Woodley Rd, Bldg B, Toledo, OH 43606

Phone: 419-474-9324
